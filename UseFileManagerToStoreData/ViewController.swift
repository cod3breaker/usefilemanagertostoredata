//
//  ViewController.swift
//  UseFileManagerToStoreData
//
//  Created by Sunimal Herath on 16/6/19.
//  Copyright © 2019 Sunimal Herath. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var itemArray = [Item]()
    
    // MARK - IB Outlets
    
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var itemTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let newItem = Item()
        let newItm2 = Item()
        newItem.title = "Sample Title"
        newItm2.title = "John Doe"
        newItm2.done = true
        
        itemArray.append(newItem)
        itemArray.append(newItm2)
        
        itemTableView.dataSource = self
        itemTableView.delegate = self
    }
    
    // MARK - TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
        
        var item = Item()
        item = itemArray[indexPath.row]
        
        cell.textLabel?.text = item.title
        cell.accessoryType = item.done ? .checkmark : .none
        
        print("Cell Data : \(item.title)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // If already have a checkmark, toggle it off and vise-versa. Change this in itemArray and reload the datatable with the itemArray.
        itemArray[indexPath.row].done = !itemArray[indexPath.row].done
        
        tableView.reloadData()
    }
    
    // MARK - IB Actions
    
    @IBAction func addButtonPressed(_ sender: Any) {
        let newItem = Item()
        newItem.title = titleText.text!
        itemArray.append(newItem)
        titleText.text = ""
        itemTableView.reloadData()
    }
}

