//
//  Item.swift
//  UseFileManagerToStoreData
//
//  Created by Sunimal Herath on 16/6/19.
//  Copyright © 2019 Sunimal Herath. All rights reserved.
//

import Foundation

class Item {
    var title: String = ""
    var done: Bool = false
}
